import sys

fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")
def lower_than(format1: str, format2: str) -> bool: #Find out if format1 is lower than format2 Returns True if format1 is lower, False otherwise. A format is lower than other if it is earlier in the fordered list.
    if fordered.index(format1) < fordered.index(format2):
        return True
    else:
        return False

def find_lower_pos(formats: list, pivot: int) -> int: #Find the lower format in raw", "bmp", "tiff", "ppm", "eps", "tga","pdf", "svg", "png", "gif", "jpeg", "webp"formats after pivot Returns the index of the lower format found"""
    lower: int = pivot # ahora mismo el formato mas pequeño esd el que esta en la posicion pivot (porque no hemos comprobado los demas) pero cuando encuentre otro mas pequeño, lower pasara a ser la posicion en la que este el mas pequeño
    for pos in range(pivot + 1, len(formats)): #como esta funcion te devuelve un numero entero (la posicion del formato mas pequeño) el rango tambien tiene que ser un numero
        if lower_than(formats[pos], formats[lower]):
            lower = pos
    return lower

def sort_formats(formats: list) -> list: #Sort formats list Returns the sorted list of formats, according to their position in fordered
    for pivot_pos in range(len(formats)): #el pivot no siempre es el primer numero, la pimera vez que nos recorremos la lista el formato mas pequeño se coloca en la primera posicion por lo que la segunda vezque nos recorremos la lista la primera posicion no nos importa y el pivot pasara a ser la segunda posicion de la lista
        lower_pos: int = find_lower_pos(formats, pivot_pos)  #realizara la funcion que hemos definido antes pero en vez de solo con el pivot (el numero en primera posicion), el pivot se ha cambiado a pivot_pos por lo que irsa cambiando
        formats[pivot_pos], formats[lower_pos] = formats[lower_pos], formats[pivot_pos]
    return formats

def main(): #Read command line arguments, and print them sorted Also, check if they are valid formats using the fordered tuple"""
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()

if __name__ == '__main__':
    main()
